'use strict';

const fs = require('fs');
const path = require('path');
const assert = require('../assert');
const O = require('../js-util');
const sqlite3 = require('sqlite3');

const LOG = 0

const db_ext = 'db';

const cwd = __dirname;
const proj_dir = path.join(cwd, '../..');
const data_dir = path.join(proj_dir, 'data');
const dbs_dir = path.join(data_dir, 'dbs');

class Database {
  static initialized = 0
  
  static async init(){
    if(this.initialized) return;
    
    O.mk_dir(data_dir);
    O.mk_dir(dbs_dir);
    
    this.initialized = 1;
  }
  
  static async open(db_name){
    await this.init();
    
    const db_file_name = `${db_name}.${db_ext}`;
    const db_dir = path.join(dbs_dir, db_name);
    const db_file = path.join(db_dir, db_file_name);
    const log_file = path.join(db_dir, 'log.txt');
    
    O.mk_dir(db_dir);
    
    const db_obj = new sqlite3.Database(db_file);
    
    const db = new Database({
      db_name,
      db_file_name,
      db_dir,
      log_file,
      db_file,
      db: db_obj,
    });

    await db.log('open');

    return db;
  }
  
  closed = 0;
  sem = new O.Semaphore();

  constructor(obj){
    for(const key of O.keys(obj))
      this[key] = obj[key];

    if(LOG) this.log_fd = fs.openSync(this.log_file, 'a');
  }

  async log(type, info=null){
    if(!LOG) return

    fs.writeSync(this.log_fd, '\n' + JSON.stringify({
      date: (new Date()).toUTCString(),
      type,
      info,
    }));
  }

  async close(){
    if(this.closed) return;
    this.closed = 1;

    await this.log('close');
    
    this.db.close();
    if(LOG) fs.closeSync(this.log_fd);
  }
  
  async atomic(fn){
    const {sem} = this;
    
    // await this.log('atomic_start');
    await sem.wait();
    const promise = typeof fn === 'function' ? fn() : fn;
    const res = await promise;
    sem.signal();
    // await this.log('atomic_end');
    
    return res;
  }
  
  async run(query){
    const res = await this.atomic(new Promise((res, rej) => {
      if(this.closed) return;
      const {db} = this;

      db.exec(query, err => {
        if(err !== null) return rej(err);
        res();
      });
    }));

    await this.log('run', {query});
    return res;
  }
  
  async all(query){
    const res = await this.atomic(new Promise((res, rej) => {
      if(this.closed) return;
      const {db} = this;
      
      db.all(query, (err, rows) => {
        if(err !== null) return rej(err);
        res(rows);
      });
    }));

    await this.log('all', {query});
    return res;
  }
  
  async prepare(query){
    const res = await this.atomic(new Promise((res, rej) => {
      if(this.closed) return;
      const {db} = this;
      
      const st = db.prepare(query, err => {
        if(err !== null) return rej(err);
        res(new Statement(this, st));
      });
    }));

    await this.log('prepare', {query});
    return res;
  }
  
  async has_table(name){
    const st = await this.prepare(`
      SELECT name FROM sqlite_master
      WHERE type = 'table' AND name = ?
    `);
  
    const xs = await st.all([name]);
    await st.finalize();
    
    return xs.length !== 0;
  }
  
  async get_cols(name){
    const st = await this.prepare(`
      select * from pragma_table_info(?)
    `);

    const xs = await st.all([name]);
    await st.finalize();
    
    return xs;
  }
  
  async get_fk_cols(name){
    const st = await this.prepare(`
      select * from pragma_foreign_key_list(?)
    `);

    const xs = await st.all([name]);
    await st.finalize();
    
    return xs;
  }
}

class Statement {
  constructor(db, st){
    this.db = db;
    this.st = st;
  }
  
  get closed(){
    return this.db.closed;
  }

  async log(type, info){
    return this.db.log(`stat_${type}`, info);
  }
  
  async run(args=[]){
    const res = await this.db.atomic(new Promise((res, rej) => {
      if(this.closed) return;
      const {st} = this;
      
      st.run(...args, err => {
        if(err !== null) return rej(err);
        res();
      });
    }));

    await this.log('run', args);
    return res;
  }
  
  async all(args=[]){
    const res = await this.db.atomic(new Promise((res, rej) => {
      if(this.closed) return;
      const {st} = this;
      
      st.all(...args, (err, rows) => {
        if(err !== null) return rej(err);
        res(rows);
      });
    }));

    await this.log('all', args);
    return res;
  }
  
  async finalize(){
    const res = await this.db.atomic(new Promise((res, rej) => {
      if(this.closed) return;
      const {st} = this;
      
      st.finalize(() => {
        res();
      });
    }));

    await this.log('finalize');
    return res;
  }
}

module.exports = Object.assign(Database, {
  Statement,
});