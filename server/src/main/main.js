"use strict"

const fs = require("fs")
const path = require("path")
const O = require("../js-util")
const express = require("express")
const cors = require("cors")
const bodyParser = require("body-parser")
const Database = require("../db")
const readline = require('../readline');
const router = require("./router")
const controller = require("./controller")

const db_name = "main"
const port = 4000

const cwd = __dirname;
const sql_dir = path.join(cwd, "db");
const sql_init_file = path.join(sql_dir, "init.sql");

let closed = 0

let rl, db, app, server

const main = async () => {
  rl = readline.rl()
  rl.on("line", onInput)

  db = await Database.open(db_name)

  controller.setDb(db)

  // const [{n}] = await db.all(`select count(*) as n from user`)
  // if(n < 100){
  //   for(let i = 0; i !== 100; i++)
  //     await controller.addUser(`user${i}`, 'a')
  // }
  // return

  if(!await db.has_table("state")){
    // Init database
    
    const sql = O.rfs(sql_init_file, 1);
    await db.run(sql);

    await controller.addUser("admin", "pass")

    await db.run(`
      update user
      set approved = 1, is_admin = 1
      where username = 'admin'
    `)
  }

  // log(await db.all(`
  //   select S.song, S.time_offset, S.playing, P.name
  //   from state S, path P
  //   where P.id = S.pth
  // `));

  app = express()
  
  app.use(cors())
  // app.use(bodyParser.urlencoded({extended: false}))
  app.use(bodyParser.json())
  app.use("/", router)

  server = app.listen(port, () => {
    log(`Server is listening on port ${port}`)
  })
}

const onInput = s => {
  if(closed) return;

  if(s === ""){
    closed = 1

    rl.close()
    server.close()
    
    return
  }
}

main().catch(log)