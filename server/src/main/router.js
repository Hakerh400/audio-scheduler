"use strict"

const fs = require("fs")
const path = require("path")
const O = require("../js-util")
const express = require("express")
const bodyParser = require("body-parser")
const controller = require("./controller")

const mk_router = () => {
  const router = express.Router()

  router.post("/signup", controller.signup)
  router.post("/login", controller.login)
  router.post("/logout", controller.logout)
  router.post("/get_users", controller.getUsers)
  router.post("/get_user_info", controller.getUserInfo)
  router.post("/approve_user", controller.approveUser)

  return router
}

module.exports = mk_router()