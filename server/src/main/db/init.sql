begin transaction;

create table "user" (
	"id" integer,
	"username" text,
	"pw_salt" text,
	"pw_hash" text,
	"token" text,
	"is_admin" integer not null default 0,
	"approved" integer not null default 0,
	primary key("id" autoincrement)
);

create table "author" (
	"id" integer,
	"name" text not null unique,
	primary key("id" autoincrement)
);

create table "song" (
	"id" integer,
	"author" integer,
	"name" text,
	"ytid" text,
	"hash" text,
	foreign key("author") references "author"("id"),
	primary key("id" autoincrement)
);

create table "playlist" (
	"song" integer not null,
	foreign key("song") references "song"("id")
);

create table "state" (
	"user" integer not null,
	"song" integer,
	"time_offset" integer,
	"playing" integer,
	foreign key("user") references "user"("id"),
	foreign key("song") references "song"("id")
);

commit;