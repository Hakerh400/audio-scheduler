"use strict"

const fs = require("fs")
const path = require("path")
const crypto = require("crypto")
const O = require("../js-util")
const express = require("express")
const bodyParser = require("body-parser")
const Database = require("../db")

const {min, max, floor, ceil} = Math

const resultsPerPage = 50

const sem = new O.Semaphore(1)

let db = null

const controller = {
  setDb(db1){
    db = db1
  },

  async addUser1(username, pw){
    try{
      let st = await db.prepare(`
        select id from user U
        where U.username = ?
      `)
      const hasUser = (await st.all([username])).length !== 0
      await st.finalize()

      if(hasUser)
        return 'username_taken'

      const pw_salt = crypto.randomBytes(8)
      const pw_inp_buf = Buffer.concat([
        pw_salt, Buffer.from(pw),
      ])
      const pw_hash = crypto.createHash('sha512')
        .update(pw_inp_buf).digest()
      
      const pw_salt_hex = pw_salt.toString("base64")
      const pw_hash_hex = pw_hash.toString("base64")

      st = await db.prepare(`
        insert into user (username, pw_salt, pw_hash)
        values (?, ?, ?)
      `)
      await st.run([username, pw_salt_hex, pw_hash_hex])
      await st.finalize()

      return null
    }catch(e){
      return e
    }
  },

  async addUser(username, pw){
    const e = await controller.addUser1(username, pw)
    if(e) throw e
  },

  async signup(req, res){
    await sem.wait()
    
    try{
      const {body} = req
      const {user: username, pw} = body

      await controller.addUser(username, pw)

      res.json({msg: "ok"})
    }catch(e){
      if(e instanceof Error){
        log(e)
        e = e.stack
      }

      res.json({err: e})
    }finally{
      sem.signal()
    }
  },

  async login(req, res){
    await sem.wait()
    
    try{
      const {body} = req
      const {user: username, pw} = body

      let st = await db.prepare(`
        select id, pw_salt, pw_hash, approved, is_admin
        from user U
        where U.username = ?
          and U.pw_salt is not null
          and U.pw_hash is not null
      `)
      const xs = await st.all([username])
      await st.finalize()

      const dfltErr = () => {
        res.json({err: "wrong_username_or_pw"})
      }

      if(xs.length === 0)
        return dfltErr()

      let {id, pw_salt, pw_hash, approved, is_admin} = xs[0]

      if(!approved)
        return res.json({err: "not_approved"})

      pw_salt = Buffer.from(pw_salt, "base64")
      pw_hash = Buffer.from(pw_hash, "base64")

      const pw_inp_buf = Buffer.concat([
        pw_salt, Buffer.from(pw),
      ])
      const pw_hash1 = crypto.createHash("sha512")
        .update(pw_inp_buf).digest()

      if(!pw_hash1.equals(pw_hash))
        return dfltErr()

      const token = crypto.randomBytes(16)
        .toString("base64")

      st = await db.prepare(`
        update user
        set token = ?
        where id = ?
      `)
      await st.run([token, id])
      await st.finalize()

      res.json({token, id, username, is_admin})
    }catch(e){
      log(e)
      res.json({err: e.stack})
    }finally{
      sem.signal()
    }
  },

  async logout(req, res){
    await sem.wait()

    try{
      const {body} = req
      const {token} = body

      let st = await db.prepare(`
        select id from user
        where token = ?
      `)
      const xs = await st.all([token])
      await st.finalize()

      if(xs.length === 0)
        return res.json({err: "invalid_token"})

      const {id} = xs[0]

      st = await db.prepare(`
        update user
        set token = null
        where id = ?
      `)
      await st.run([id])
      await st.finalize()

      res.json({msg: "ok"})
    }catch(e){
      log(e)
      res.json({err: e.stack})
    }finally{
      sem.signal()
    }
  },

  async getUserInfo(req, res){
    await sem.wait()

    try{
      const {body} = req
      const {token} = body
      const {id: userId} = body

      let st = await db.prepare(`
        select id, is_admin
        from user
        where token = ?
      `)
      let xs = await st.all([token])
      await st.finalize()
      
      if(xs.length === 0)
        return res.json({err: "invalid_token"})

      const {id, is_admin} = xs[0]

      st = await db.prepare(`
        select id, username, is_admin, approved,
          token is not null as logged_in
        from user
        where id = ?
      `)
      xs = await st.all([userId])
      await st.finalize()

      if(xs.length === 0)
        return res.json({err: "not_found"})
      
      const info = xs[0]
      const {approved} = info
      
      if(!is_admin && !approved)
        return res.json({err: "not_found"})

      return res.json(info)
    }catch(e){
      log(e)
      res.json({err: e.stack})
    }finally{
      sem.signal()
    }
  },

  async getUsers(req, res){
    await sem.wait()

    try{
      const {body} = req
      const {token} = body

      let st = await db.prepare(`
        select id, is_admin
        from user
        where token = ?
      `)
      let xs = await st.all([token])
      await st.finalize()
      
      if(xs.length === 0)
        return res.json({err: "invalid_token"})

      const {id, is_admin} = xs[0]

      let {
        searchQuery, order, orderDir: orderDirRaw,
        pageIndex,
      } = body

      searchQuery = !searchQuery ? "%" : `%${searchQuery}%`

      const orderColMp = {
        reg_time: "id",
        username: "username",
      }

      const orderDirMp = {
        asc: "asc",
        desc: "desc",
      }

      const orderCol = orderColMp[order]
      const orderDir = orderDirMp[orderDirRaw]

      if(is_admin){
        st = await db.prepare(`
          select count(*) as n
          from user
          where username like ?
        `)
        const [{n: usersNum}] = await st.all([
          searchQuery,
        ])
        await st.finalize()

        const pagesNum = max(1, ceil(usersNum / resultsPerPage))
        const resultsOffset = (pageIndex - 1) * resultsPerPage
        
        st = await db.prepare(`
          select id, username, is_admin, approved,
            token is not null as logged_in
          from user
          where username like ?
          order by ${orderCol} ${orderDir}
          limit ?, ?
        `)
        const users = await st.all([
          searchQuery, resultsOffset, resultsPerPage,
        ])
        await st.finalize()

        return res.json({users, pagesNum})
      }

      st = await db.prepare(`
        select id, username, is_admin
        from user
        where approved = 1
      `)
      const users = await st.all()
      await st.finalize()

      if(xs.length === 0)
        return res.json({err: "invalid_token"})

      return res.json({users})
    }catch(e){
      log(e)
      res.json({err: e.stack})
    }finally{
      sem.signal()
    }
  },

  async approveUser(req, res){
    await sem.wait()

    try{
      const {body} = req
      const {token, id: user_id} = body

      let st = await db.prepare(`
        select id, is_admin
        from user
        where token = ?
      `)
      let xs = await st.all([token])
      await st.finalize()

      if(xs.length === 0)
        return res.json({err: "invalid_token"})

      const {id, is_admin} = xs[0]

      if(!is_admin)
        return res.json({err: "access_denied"})

      st = await db.prepare(`
        update user
        set approved = 1
        where id = ?
      `)
      const users = await st.run([user_id])
      await st.finalize()

      res.json({msg: "ok"})
    }catch(e){
      log(e)
      res.json({err: e.stack})
    }finally{
      sem.signal()
    }
  },
}

module.exports = controller