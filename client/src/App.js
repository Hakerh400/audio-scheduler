import React, { useState } from "react"

import RegisterForm from "./RegisterForm"
import LoginForm from "./LoginForm"
import HomePage from "./HomePage"

import "./index.css"

// const UserCtx = React.createContext()

export default function App(){
  let [token, setToken] = useState(localStorage.token)

  return (<>
    {!token && <>
      <LoginForm setToken={setToken}/>
      <RegisterForm/>
    </>}
    {token &&
      // <UserCtx.Provider state={{token, setToken}}>
      <HomePage token={token} setToken={setToken}/>
      // </UserCtx.Provider>
    }
  </>)
}