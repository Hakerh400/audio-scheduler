import Button from "./Button"

export default function PageNav(props){
  const {pagesNum, pageIndex, setPageIndex} = props

  return (
    <div className="page-nav">
      <div className="page-nav-lab">Page:</div>
      {Array(pagesNum).fill().map((_, i) => {
        const index = i + 1

        return (
          <Button key={index}
            lab={index}
            enabled={index !== pageIndex}
            onClick={() => setPageIndex(index)}
          />
        )
      })}
    </div>
  )
}