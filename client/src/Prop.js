export default function Prop({p}){
  const p_str = p ? "True" : "False"
  const c_str = `prop prop-${p ? "true" : "false"}`

  return (
    <div className={c_str}>
      {p_str}
    </div>
  )
}