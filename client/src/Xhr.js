import axios from "axios"

import LocalStorage from "./LocalStorage"

const post = async (method, data, cb) => {
  const res = await axios.post(`http://localhost:4000/${method}`, data)

  try{
    const {data} = res
    await cb(data)
  }catch(err){
    if(typeof err !== "string"){
      console.log(err)
      return
    }

    if(err === "invalid_token"){
      LocalStorage.clear()
      window.location.reload()
    }
  }
}

const Xhr = {
  post,
}

export default Xhr