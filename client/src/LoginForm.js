import { useState } from "react"

import Xhr from "./Xhr"
import LocalStorage from "./LocalStorage"
import TextInput from "./TextInput"
import Button from "./Button"

export default function LoginForm({setToken}){
  let [user, setUser] = useState("")
  let [pw, setPw] = useState("")
  let [errMsg, setErrMsg] = useState("")
  let [submitted, setSubmitted] = useState(0)

  const clearErrMsg = () => {
    setErrMsg("")
  }

  const mkSetFn = fn => {
    return val => {
      clearErrMsg()
      fn(val)
    }
  }

  const onClick = () => {
    clearErrMsg()
    setSubmitted(1)

    Xhr.post("login", {
      user, pw,
    }, data => {
      if(data.err){
        setErrMsg(data.err)
        return
      }

      const {token, id, username, is_admin} = data

      LocalStorage.set("token", token)
      LocalStorage.set("userId", id)
      LocalStorage.set("username", username)
      LocalStorage.set("isAdmin", is_admin)

      setToken(token)
    }).then(() => {
      setSubmitted(0)
    })
  }

  const enabled = !submitted

  return (
    <div className="form">
      <TextInput lab="Username" enabled={enabled}
        val={user} setVal={mkSetFn(setUser)}
      />
      <TextInput lab="Password" enabled={enabled} pw={1}
        val={pw} setVal={mkSetFn(setPw)}
      />
      {errMsg && <div className="err-msg">{errMsg}</div>}
      <Button lab="Submit" enabled={enabled}
        onClick={onClick}
      />
    </div>
  )
}