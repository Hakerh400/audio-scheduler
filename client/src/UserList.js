import { useState, useEffect, useCallback } from "react"

import Xhr from "./Xhr"
import PageNav from "./PageNav"
import UserCard from "./UserCard"

export default function UserList({token, pageParams, openPage}){
  let [users, setUsers] = useState(null)
  let [pagesNum, setPagesNum] = useState(1)
  let [pageIndex, setPageIndex] = useState(1)

  let [searchQuery, setSearchQuery] = useState("")
  let [order, setOrder] = useState("reg_time")
  let [orderDir, setOrderDir] = useState("asc")

  const resetPageIndex = () => {
    setPageIndex(1)
  }

  const mkResetPageIndexFn = fn => val => {
    fn(val)
    resetPageIndex()
  }

  const params = {
    searchQuery,
    order,
    orderDir,
    setSearchQuery: mkResetPageIndexFn(setSearchQuery),
    setOrder: mkResetPageIndexFn(setOrder),
    setOrderDir: mkResetPageIndexFn(setOrderDir),
  }

  const updateUsers = useCallback(() => {
    Xhr.post("get_users", {
      token,
      searchQuery,
      order,
      orderDir,
      pageIndex,
    }, data => {
      if(data.err) throw data.err

      setUsers(data.users)
      setPagesNum(data.pagesNum)
    })
  }, [
    token, pageIndex,
    searchQuery, order, orderDir,
  ])

  useEffect(() => {
    updateUsers()
  }, [updateUsers])

  const pageNav = <PageNav
    pagesNum={pagesNum}
    pageIndex={pageIndex}
    setPageIndex={setPageIndex}
  />

  return (<div className="user-list">
    <div className="search-bar">
      Search: <input type="text" value={params.searchQuery}
        onInput={e => params.setSearchQuery(e.target.value)}
      />
      Order by: <select value={params.order}
        onChange={e => params.setOrder(e.target.value)}>
        <option value="reg_time">Registration time</option>
        <option value="username">Username</option>
      </select>
      <select value={params.orderDir}
        onChange={e => params.setOrderDir(e.target.value)}>
        <option value="asc">Ascending</option>
        <option value="desc">Descending</option>
      </select>
    </div>
    {pageNav}
    {users && <div className="user-cards">
      {users.map(user => <UserCard
        key={user.id}
        updateUsers={updateUsers}
        token={token}
        info={user}
        openPage={openPage}
      />)}
    </div>}
    {pageNav}
  </div>)
}