import { useState } from "react"

import Xhr from "./Xhr"
import TextInput from "./TextInput"
import Button from "./Button"

export default function RegisterForm(){
  let [user, setUser] = useState("")
  let [pw, setPw] = useState("")
  let [pw1, setPw1] = useState("")
  let [errMsg, setErrMsg] = useState("")
  let [submitted, setSubmitted] = useState(0)

  const clearErrMsg = () => {
    setErrMsg("")
  }

  const mkSetFn = fn => {
    return val => {
      clearErrMsg()
      fn(val)
    }
  }

  const onClick = () => {
    clearErrMsg()

    if(pw !== pw1){
      setErrMsg("Passwords do not match")
      return
    }

    setSubmitted(1)

    Xhr.post("signup", {
      user, pw,
    }, data => {
      if(data.err){
        setErrMsg(data.err)
        return
      }

      console.log(data.msg)
    }).then(() => {
      setSubmitted(0)
    })
  }

  const enabled = !submitted

  return (
    <div className="form">
      <TextInput lab="Username" enabled={enabled}
        val={user} setVal={mkSetFn(setUser)}
      />
      <TextInput lab="Password" enabled={enabled} pw={1}
        val={pw} setVal={mkSetFn(setPw)}
      />
      <TextInput lab="Confirm password" enabled={enabled}
        pw={1} val={pw1} setVal={mkSetFn(setPw1)}
      />
      {errMsg && <div className="err-msg">{errMsg}</div>}
      <Button lab="Submit" enabled={enabled}
        onClick={onClick}
      />
    </div>
  )
}