export default function TextInput({pw=0, enabled=1, lab, val, setVal}){
  const inp_type = pw ? "password" : "text"

  return (
    <div className="form-field-row">
      <div className="form-field-cell">
        <div className="text-input-lab">{lab}:</div>
      </div>
      <div className="form-field-cell">
        <input className="text-input-inp" disabled={!enabled}
          type={inp_type}
          value={val}
          onChange={a => setVal(a.target.value)}
          />
      </div>
    </div>
  )
}