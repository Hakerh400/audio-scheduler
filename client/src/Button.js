export default function Button({lab, enabled=1, onClick}){
  return (
    <button className="btn btn-submit" disabled={!enabled}
      onClick={onClick}
    >{lab}</button>
  )
}