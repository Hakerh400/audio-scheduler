import { useState } from "react"

import Xhr from "./Xhr"
import LocalStorage from "./LocalStorage"
import Button from "./Button"
import UserList from "./UserList"
import UserPage from "./UserPage"
import AddSongPage from "./AddSongPage"

export default function UserHome({token, setToken}){
  let [btnLogoutEnabled, setBtnLogoutEnabled] = useState(1)
  // let [page, setPage] = useState("userList")
  // let [pageParams, setPageParams] = useState({})
  let [page, setPage] = useState("addSong")
  let [pageParams, setPageParams] = useState({})

  const username = LocalStorage.get("username")

  const logOut = () => {
    setBtnLogoutEnabled(0)

    Xhr.post("logout", {
      token,
    }, data => {
      if(data.err) throw data.err

      LocalStorage.remove("token")
      LocalStorage.remove("userId")
      LocalStorage.remove("username")
      LocalStorage.remove("isAdmin")

      setToken(null)
      setBtnLogoutEnabled(1)
    })
  }

  const openPage = (page, pageParams={}) => {
    setPage(page)
    setPageParams(pageParams)
  }

  const mkPageFn = Comp => () => {
    return <Comp
      token={token}
      pageParams={pageParams}
      openPage={openPage}
    />
  }

  const pagesObj = {
    userList: mkPageFn(UserList),
    userPage: mkPageFn(UserPage),
    addSong: mkPageFn(AddSongPage),
  }

  const pagesInfo = [
    ["userList", "Users"],
    // ["userPage", "User page"],
    ["addSong", "Add [s]"],
  ]

  return (
    <div className="user-home">
      <div className="cur-user-name">
        Logged in as <b>{username}</b>
      </div>
      <div style={{marginTop: "10px"}}/>
      <Button
        lab="Log out"
        enabled={btnLogoutEnabled}
        onClick={logOut}
      />
      <br/>
      {pagesInfo.map(([page1, lab]) => <Button
        key={page1}
        lab={lab}
        onClick={() => setPage(page1)}
      />)}
      {pagesObj[page]()}
    </div>
  )
}