import { useState } from "react"

import Xhr from "./Xhr"
import Prop from "./Prop"
import Button from "./Button"
import Link from "./Link"
import LocalStorage from "./LocalStorage"

export default function UserCard({updateUsers, token, info, openPage}){
  const {id, username} = info;

  let [btnApproveEnabled, setBtnApproveEnabled] = useState(1)

  const userId = LocalStorage.getInt("userId")
  const isAdmin = LocalStorage.getProp("isAdmin")

  const approve = () => {
    setBtnApproveEnabled(0)

    Xhr.post("approve_user", {
      token,
      id,
    }, data => {
      if(data.err) throw data.err

      updateUsers()
    }).then(() => {
      setBtnApproveEnabled(1)
    })
  }

  const openUserPage = () => {
    openPage("userPage", {id})
  }

  const isCurUser = info.id === userId
  const c_str = `user-card${isCurUser ? " user-card-cur-user" : ""}`

  return (
    <div className={c_str}>
      <Link className="user-card-username"
        lab={username}
        onClick={openUserPage}
      />
      <div className="user-card-prop user-card-prop-is-admin">
        Administrator:<Prop p={info.is_admin}/></div>
      {isAdmin && <>
        <div className="user-card-prop user-card-prop-approved">
          Approved:<Prop p={info.approved}/></div>
        <div className="user-card-prop user-card-prop-logged-in">
          Logged in:<Prop p={info.logged_in}/></div>
        {!info.approved && <Button
          lab="Approve"
          enabled={btnApproveEnabled}
          onClick={approve}
        />}
      </>}
    </div>
  )
}