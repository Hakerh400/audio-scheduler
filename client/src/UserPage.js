import { useState, useEffect, useCallback } from "react"

import Xhr from "./Xhr"
import LocalStorage from "./LocalStorage"
import Prop from "./Prop"
import Button from "./Button"
import Link from "./Link"

export default function UserPage({token, pageParams, openPage}){
  const {id} = pageParams

  const username = LocalStorage.get("username")
  const isAdmin = LocalStorage.getProp("isAdmin")

  const [info, setInfo] = useState(null)
  let [btnApproveEnabled, setBtnApproveEnabled] = useState(1)

  const updateUserInfo = useCallback(() => {
    Xhr.post("get_user_info", {
      token,
      id,
    }, data => {
      if(data.err) throw data.err

      setInfo(data)
    })
  }, [token, id])

  useEffect(() => {
    updateUserInfo()
  }, [updateUserInfo])

  const approve = () => {
    setBtnApproveEnabled(0)

    Xhr.post("approve_user", {
      token,
      id,
    }, data => {
      if(data.err) throw data.err

      updateUserInfo()
    }).then(() => {
      setBtnApproveEnabled(1)
    })
  }

  if(info === null) return

  return (<>
    <Link className="user-card-username"
      lab={username}
      onClick={null}
    />
    <div className="user-card-prop user-card-prop-is-admin">
      Administrator:<Prop p={info.is_admin}/></div>
    {isAdmin && <>
      <div className="user-card-prop user-card-prop-approved">
        Approved:<Prop p={info.approved}/></div>
      <div className="user-card-prop user-card-prop-logged-in">
        Logged in:<Prop p={info.logged_in}/></div>
      {!info.approved && <Button
        lab="Approve"
        enabled={btnApproveEnabled}
        onClick={approve}
      />}
    </>}
  </>)
}