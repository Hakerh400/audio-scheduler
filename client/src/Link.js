export default function Link({className, lab, onClick}){
  const c_str = `link${className ? ` ${className}` : ""}`

  return (
    <button
      className={c_str}
      onClick={onClick}
    >{lab}</button>
  )
}