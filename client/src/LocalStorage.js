const has = key => {
  return localStorage.hasOwnProperty(key)
}

const get = key => {
  if(!has(key)) return null
  return localStorage[key]
}

const getStr = key => {
  return get(key)
}

const getInt = key => {
  const val = get(key)
  if(val === null) return null
  return Number(val)
}

const getProp = key => {
  const val = getInt(key)
  if(val === null) return null
  return Boolean(val)
}

const set = (key, val) => {
  localStorage[key] = val
}

const remove = key => {
  delete localStorage[key]
}

const getKeys = () => {
  return Reflect.ownKeys(localStorage)
}

const clear = () => {
  for(const key of getKeys())
    remove(key)
}

const LocalStorage = {
  has,
  get,
  getStr,
  getInt,
  getProp,
  set,
  remove,
  getKeys,
  clear,
}

export default LocalStorage